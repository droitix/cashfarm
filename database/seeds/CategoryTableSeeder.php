<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
           [

                'name' => '3 Days',
                'percent' => '1.3',
                'color' => '3',
                'icon' => 'fa-laptop',
                'children' => [

                    ['name' => '30% in 3 Days '],


                     ]
            ],
            [
                'name' => '6 Days',
                'percent' => '1.50',
                'color' => '6',
                'icon' => 'fa-university',
                'children' => [
                    ['name' => '50% in 6 Days'],

                    ]
            ],

   
            

        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}
