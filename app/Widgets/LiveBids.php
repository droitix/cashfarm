<?php

namespace openjobs\Widgets;

use Arrilot\Widgets\AbstractWidget;
use openjobs\{Area, Category, Listing};
use Illuminate\Http\Request;
use Auth;



class LiveBids extends AbstractWidget
{

    public $reloadTimeout = 5;
    /**
     * The configuration array.
     *
     * @var array
     */


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */


    public function run(Area $area, Listing $listing)
    {

$listings = Listing::all()->where('live',true);


$now = \Carbon\Carbon::now();
$morningstart = '02:00:00';
$morningend = '12:10:00';
$eveningstart = '20:00:00';
$eveningend = '23:00:00';
$time = $now->format('H:i:s');




        return view('widgets.live_bids', compact('listings','morningstart','morningend','time','eveningstart','eveningend'));
    }
}
