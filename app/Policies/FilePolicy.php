<?php

namespace openjobs\Policies;

use openjobs\{User, File};
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    public function destroy(User $user, File $file)
    {
        return $this->touch($user, $file);
    }

     public function touch(User $user, File $file)
    {
        return $file->ownedByUser($user);
    }
}
