<?php

namespace openjobs\Http\Controllers\Resume;

use openjobs\{Area, Category, Resume};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use openjobs\Http\Controllers\Controller;
use openjobs\Jobs\UserViewedResume;
use openjobs\Http\Requests\StoreResumeFormRequest;

class ResumeController extends Controller
{
    public function index(Category $category)
    {
        $resumes = Resume::with(['user', 'area'])->isLive()->fromCategory($category)->latestFirst()->paginate(15);



        return view('resumes.index', compact('resumes', 'category'));

    }



    public function show(Request $request, Area $area, Resume $resume)
    {
        if (!$resume->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedResume($request->user(), $resume));
        }

        return view('resumes.show', compact('resume'));
    }

      public function reveal(Request $request, Area $area, Resume $resume)
    {
        if (!$resume->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedResume($request->user(), $resume));
        }

        return view('resumes.reveal', compact('resume'));
    }


    public function create(Resume $resume)
    {
        return view('resumes.create',compact('resume'));
    }




    public function store(StoreResumeFormRequest $request, Area $area)
    {
        $resume = new Resume;
        $resume->edulevel1 = $request->edulevel1;
        $resume->edutitle1 = $request->edutitle1;
        $resume->edufromyear1 = $request->edufromyear1;
        $resume->edutoyear1 = $request->edutoyear1;
        $resume->institution1 = $request->institution1;
        $resume->comp1 = $request->comp1;
        $resume->comp1title = $request->comp1title;
        $resume->comp1loc = $request->comp1loc;
        $resume->compfromyear1 = $request->compfromyear1;
        $resume->comptoyear1 = $request->comptoyear1;
        $resume->nxtjobtitle = $request->nxtjobtitle;
        $resume->nxtjobrelocate = $request->nxtjobrelocate;
        $resume->nxtjobtype = $request->nxtjobtype;
        $resume->category_id = $request->category_id;
        $resume->area_id = $request->area_id;
        $resume->user()->associate($request->user());


        $resume->live = true;
        $resume->created_at = \Carbon\Carbon::now();
        $resume->save();

        notify()->success('Resume created continue editing it!');

        return redirect()
            ->route('resumes.show', [$area, $resume])
            ->withSuccess('Your resume is now created continue editing it.');
    }

    public function edit(Request $request, Area $area, Resume $resume)
    {
        $this->authorize('edit', $resume);

        return view('resumes.edit', compact('resume'));
    }

     public function eduedit(Request $request, Area $area, Resume $resume)
    {
        $this->authorize('edit', $resume);

        return view('resumes.eduedit', compact('resume'));
    }


     public function compedit(Request $request, Area $area, Resume $resume)
    {
        $this->authorize('edit', $resume);

        return view('resumes.compedit', compact('resume'));
    }


    public function update(StoreResumeFormRequest $request, Area $area, Resume $resume)
    {
        $this->authorize('update', $resume);

        $resume->edulevel1 = $request->edulevel1;
        $resume->edulevel2 = $request->edulevel2;
        $resume->edulevel3 = $request->edulevel3;
        $resume->edulevel4 = $request->edulevel4;
        $resume->edutitle1 = $request->edutitle1;
        $resume->edutitle2 = $request->edutitle2;
        $resume->edutitle3 = $request->edutitle3;
        $resume->edutitle4 = $request->edutitle4;
        $resume->edufromyear1 = $request->edufromyear1;
        $resume->edufromyear2 = $request->edufromyear2;
        $resume->edufromyear3 = $request->edufromyear3;
        $resume->edufromyear4 = $request->edufromyear4;
        $resume->edutoyear1 = $request->edutoyear1;
        $resume->edutoyear2 = $request->edutoyear2;
        $resume->edutoyear3 = $request->edutoyear3;
        $resume->edutoyear4 = $request->edutoyear4;
        $resume->institution1 = $request->institution1;
        $resume->institution2 = $request->institution2;
        $resume->institution3 = $request->institution3;
        $resume->institution4 = $request->institution4;
        $resume->comp1 = $request->comp1;
        $resume->comp1title = $request->comp1title;

        $resume->comp1loc = $request->comp1loc;
        $resume->compfromyear1 = $request->compfromyear1;
        $resume->comptoyear1 = $request->comptoyear1;

        $resume->comp2 = $request->comp2;
        $resume->comp2title = $request->comp2title;

        $resume->comp2loc = $request->comp2loc;
        $resume->compfromyear2 = $request->compfromyear2;
        $resume->comptoyear2 = $request->comptoyear2;

        $resume->comp3 = $request->comp3;
        $resume->comp3title = $request->comp3title;

        $resume->comp3loc = $request->comp3loc;
        $resume->compfromyear3 = $request->compfromyear3;
        $resume->comptoyear3 = $request->comptoyear3;

        $resume->comp4 = $request->comp4;
        $resume->comp4title = $request->comp4title;

        $resume->comp4loc = $request->comp4loc;
        $resume->compfromyear4 = $request->compfromyear4;
        $resume->comptoyear4 = $request->comptoyear4;

        $resume->nxtjobtitle = $request->nxtjobtitle;
        $resume->nxtjobrelocate = $request->nxtjobrelocate;
        $resume->nxtjobtype = $request->nxtjobtype;
        $resume->category_id = $request->category_id;
        $resume->area_id = $request->area_id;
        if (!$resume->live()) {
            $resume->category_id = $request->category_id;
        }

        $resume->area_id = $request->area_id;



        $resume->save();

        if ($request->has('payment')) {
            return redirect()->route('resumes.payment.show', [$area, $resume]);
        }

        return back()->withSuccess('Resume edited successfully.');
    }

    public function destroy(Area $area, Resume $resume)
    {
        $this->authorize('destroy', $resume);

        $resume->delete();

        return back()->withSuccess('Resume was deleted.');
    }
 /**
     * Search for listings by category and keywords.
     *
     * @return \Illuminate\Http\Response
     */

}
