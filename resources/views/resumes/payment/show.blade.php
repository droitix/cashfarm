@extends('layouts.regapp')

@section('content')

    <!-- Our Pricing Table -->
    <section class="our-pricing bgc-fa">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="ulockd-main-title mb50">

                    </div>
                </div>
            </div>
            <div class="row">


                <div class="col-md-6 col-lg-4 p0 pull-right prpl5-sm">
                    <div class="pricing_table">
                        <div class="pt_header_four">
                            <div class="pt_tag_four"><span>Your Ad will now go Live</span></div>
                            <h4>Publish your Job Advert</h4>
                        </div>
                        <form action="{{ route('listings.payment.update', [$area, $listing]) }}" method="post">
                        <div class="pt_details">

                            <button class="btn btn-lg btn-block"><span> Publish!</span></button>
                        </div>

                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
