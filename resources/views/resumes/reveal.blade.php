@extends('layouts.userapp')

@section('title')
   Profile for {{$resume->user->fullname}}  | Openjobs360
@endsection

@section('content')


    <!-- Candidate Personal Info Details-->
    <section class="mt70 bgc-fa mt50">
        <div class="container">
            <div class="row candidate_grid">
                <div class="col-lg-8 col-xl-8">
                    <div class="candidate_personal_info">
                        <div class="thumb">

                        </div>
                        <div class="details">
                            <h3>{{$resume->user->fullname}}<small class="verified"><i class="fa fa-check-circle"></i></small></h3>
                            <p>{{$resume->user->occupationtitle}}</p>
                            <ul class="address_list">
                                <li class="list-inline-item"><a href="#">{{$resume->user->province}}</a></li>

                            </ul>
                            <ul class="review_list">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star-o"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4">
                    <div class="candidate_personal_overview">
                        <ul class="timer_list">
                            <li class="list-inline-item"><div>Address</div></li>
                            <li class="list-inline-item"><span></span></li>
                        </ul>
                        <p>{{$resume->user->address}}</p>
                        <ul class="skills">
                            <li class="progressbar3" data-width="85" data-target="90"></li>
                        </ul>
                        <div class="row mb10">
                            <div class="col col-sm-4 col-lg-4">
                                <div class="grid">
                                    <div class="price">Gender</div>
                                    <p>{{$resume->user->gender}}</p>
                                </div>
                            </div>
                            <div class="col col-sm-4 col-lg-4 text-center">
                                <div class="grid">
                                    <div class="total_job">Contact</div>
                                    <p>{{$resume->user->phone}}</p>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-block btn-thm"><span class="flaticon-ticket"></span> Shortlist This Candidate</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="candidate_about_info">
                                <h4 class="fz20 mb30">About Me</h4>
                                <p class="mb30">{{$resume->user->aboutme}}</p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="my_resume_eduarea style2">
                                <h4 class="title">Education History</h4>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution1}} <small>{{$resume->edufromyear1}}-{{$resume->edutoyear1}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle1}}</h4>
                                    <p class="mb0"></p>
                                </div>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->institution2}} <small>{{$resume->edufromyear2}}-{{$resume->edutoyear2}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle2}}</h4>

                                </div>
                                <div class="content">
                                    <div class="circle"></div>
                                  <p class="edu_center">{{$resume->institution3}} <small>{{$resume->edufromyear3}}-{{$resume->edutoyear3}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle3}}</h4>

                                </div>
                                <div class="content">
                                    <div class="circle"></div>
                                  <p class="edu_center">{{$resume->institution4}} <small>{{$resume->edufromyear4}}-{{$resume->edutoyear4}}</small></p>
                                    <h4 class="edu_stats">{{$resume->edutitle4}}</h4>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="my_resume_eduarea style2">
                                <h4 class="title">Work & Experience</h4>
                                <div class="content">
                                    <div class="circle"></div>
                                    <p class="edu_center">{{$resume->comp1}}<small>{{$resume->compfromyear1}}-{{$resume->comptoyear1}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp1title}}</h4>

                                </div>
                                <div class="content style2">
                                    <div class="circle"></div>
                                     <p class="edu_center">{{$resume->comp2}}<small>{{$resume->compfromyear2}}-{{$resume->comptoyear2}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp2title}}</h4>

                                </div>
                                <div class="content style2">
                                    <div class="circle"></div>
                                     <p class="edu_center">{{$resume->comp3}}<small>{{$resume->compfromyear3}}-{{$resume->comptoyear3}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp3title}}</h4>

                                </div>
                                <div class="content style2">
                                    <div class="circle"></div>
                                     <p class="edu_center">{{$resume->comp4}}<small>{{$resume->compfromyear4}}-{{$resume->comptoyear4}}</small></p>
                                    <h4 class="edu_stats">{{$resume->comp4title}}</h4>

                                </div>
                            </div>
                        </div>



                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="candidate_working_widget">
                        <div class="icon text-thm"><span class="flaticon-mortarboard"></span></div>
                        <div class="details">
                            <h4>Highest Level</h4>
                            <p>{{$resume->user->educationlevel}}</p>
                        </div>
                        <div class="icon text-thm"><span class="flaticon-controls"></span></div>
                        <div class="details">
                            <h4>Experience</h4>
                            <p>6-9 Years</p>
                        </div>
                        <div class="icon text-thm"><span class="flaticon-old-age-man"></span></div>
                        <div class="details">
                            <h4>Born</h4>
                            <p>{{$resume->user->birthdate}}</p>
                        </div>
                        <div class="icon text-thm"><span class="flaticon-paper"></span></div>
                        <div class="details">
                            <h4>Languages</h4>
                            <p>{{$resume->user->languages}}</p>
                        </div>

                    </div>
                    <div class="candidate_social_widget">
                        <ul>
                            <li>Social Profiles</li>
                            <li><a href="{{$resume->user->facebook}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$resume->user->twitter}}"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{$resume->user->instagram}}"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="{{$resume->user->linkedin}}"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <h4 class="fz20 mb30">Attachments</h4>
                    <div class="candidate_document_widget">
                        <div class="details">
                        <div class="icon"><span class="flaticon-doc"></span></div>
                            <h4 class="title">Cover Letter</h4>
                            <p>PDF</p>
                        </div>
                    </div>
                    <div class="candidate_document_widget">
                        <div class="icon"><span class="flaticon-doc"></span></div>
                        <div class="details">
                            <h4 class="title">Contrac</h4>
                            <p>DOCX</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>



@endsection
