<tr>
                                     <th scope="row">
                                                    <img class="img-fluid" src="/images/partners/10.jpg" alt="10.jpg">
                                                </th>
                                                <td>
                                                    <h4>{{ $listing->jobtitle }}</h4>
                                                    <p>{{ $listing->companyname }}</p>
                                                    <ul>
                                                        <li class="list-inline-item"><a href="#"><span class="flaticon-location-pin"></span></a></li>
                                                        <li class="list-inline-item"><a href="#">{{ $listing->area->parent->name }}, {{ $listing->area->name }}</a></li>
                                                    </ul>
                                                </td>
                                                <td></td>
                                                <td>{{ $listing->created_at->diffForHumans() }}</td>
                                                <td>
                                                    <ul class="view_edit_delete_list">
                                                        <li class="list-inline-item"><a href="{{ route('listings.show', [$area, $listing]) }}" data-toggle="tooltip" data-placement="top" title="View"><span class="flaticon-eye"></span></a></li>


                                                    </ul>
                                                </td>
                                            </tr>


