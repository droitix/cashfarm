<tr class="mb30">
                                                <th scope="row">
                                                    <ul>
                                                        <li class="list-inline-item"><a href="{{ route('resumes.reveal', [$area, $resume]) }}"><span class="flaticon-resume font"></span></a></li>
                                                        <li class="list-inline-item cv_sbtitle"><a href="{{ route('resumes.reveal', [$area, $resume]) }}">Resume for {{ $resume->user->fullname }}</a></li>
                                                    </ul>
                                                </th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <ul class="view_edit_delete_list">
                                                         <li class="list-inline-item"><a href="{{ route('resumes.reveal', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="View"><span class="flaticon-eye"></span></a></li>
                                                        <li class="list-inline-item"><a href="{{ route('resumes.show', [$area, $resume]) }}" data-toggle="tooltip" data-placement="top" title="Edit"><span class="flaticon-edit"></span></a></li>
                                                        <li class="list-inline-item"><a href="#" data-toggle="tooltip" data-placement="top" title="Delete"><span class="flaticon-rubbish-bin"></span></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
