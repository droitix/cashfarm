@extends('layouts.app')

@section('title', 'Bid')

@section('description')

@endsection

@section('content')



 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row layout-top-spacing">

                               <div id="basic" class="col-lg-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">


                                            <h4>You must pay R{{$comment->split}} to the details below</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">

                                    <div class="row">
                                        <div class="col-lg-6 col-12 mx-auto">
                                            <h4>Name--{{$comment->listing->user->name}} {{$comment->listing->user->surname}}</h4>
                                           <h4>Bank--{{$comment->listing->user->bank}}</h4>
                                           <h4>Account No--{{$comment->listing->user->account}}</h4>
                                         
                                           <h4>Phone--{{$comment->listing->user->phone}}</h4>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

            </div>
        </div>

        </div>
        <!--  END CONTENT PART  -->

    </div>
    <!-- END MAIN CONTAINER -->


@endsection
