<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from designreset.com/cork/ltr/demo3/table_dt_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Sep 2020 15:56:51 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>DataTables Basic | CORK - Multipurpose Bootstrap Dashboard Template </title>
    <link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="/plugins/table/datatable/dt-global_style.css">
    <!-- END PAGE LEVEL STYLES -->

</head>
<body>

    <!--  BEGIN NAVBAR  -->
    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm">

            <ul class="navbar-item theme-brand flex-row  text-center">
                <li class="nav-item theme-logo">
                    <a href="{{url('/')}}">
                        <img src="/assets/img/logo.svg" class="navbar-logo" alt="logo">
                    </a>
                </li>
                <li class="nav-item theme-text">
                    <a href="{{url('/')}}" class="nav-link"> GLAMOUR COIN AUCTION </a>
                </li>
            </ul>



            <ul class="navbar-item flex-row ml-md-auto">






                <li class="nav-item dropdown user-profile-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="/assets/img/profile-16.jpg" alt="avatar">
                    </a>
                    <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                        <div class="">
                            <div class="dropdown-item">
                                <a href="{{url('profile')}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> My Profile</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="apps_mailbox.html"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-inbox"><polyline points="22 12 16 12 14 15 10 15 8 12 2 12"></polyline><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path></svg> Inbox</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="auth_lockscreen.html"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg> Lock Screen</a>
                            </div>
                            <div class="dropdown-item">
                                <a href="auth_login.html"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg> Sign Out</a>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

     <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><span>Analytics</span></li>
                            </ol>
                        </nav>

                    </div>
                </li>
            </ul>
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <a class="dropdown-toggle btn" href="#" role="button" id="customDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Settings</span> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customDropdown">
                            <a class="dropdown-item" data-value="Settings" href="javascript:void(0);">Settings</a>
                            <a class="dropdown-item" data-value="Mail" href="javascript:void(0);">Mail</a>
                            <a class="dropdown-item" data-value="Print" href="javascript:void(0);">Print</a>
                            <a class="dropdown-item" data-value="Download" href="javascript:void(0);">Download</a>
                            <a class="dropdown-item" data-value="Share" href="javascript:void(0);">Share</a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        @include('layouts.partials.sidebar')
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

               
                <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col-sm-4">
                                                <div class="search-box mr-2 mb-2 d-inline-block">
                                                    <div class="position-relative">
                                                        <input type="text" class="form-control" placeholder="Search...">
                                                        <i class="bx bx-search-alt search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="text-sm-right">
                                                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 mr-2"><i class="mdi mdi-plus mr-1"></i> Bids</button>
                                                </div>
                                            </div><!-- end col-->
                                        </div>
 <div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead>
                                                    <tr>

                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Amount</th>
                                                        <th>Bank</th>
                                                        <th>Type</th>
                                                        <th>Creation Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      @foreach ($listings as $listing)




                                        <tr>
                                             @if (!$listing->type())
                                             @if (!$listing->matched())
                                             <td style="background-color: yellow">{{$listing->id}} PH REQUEST

                                            </td>
                                            @else
                                             <td style="background-color: yellow">{{$listing->id}} PH (MATCHED)

                                            </td>
                                            @endif
                                            <td style="background-color: yellow">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: yellow">{{$listing->user->email}}</td>
                                            <td style="background-color: yellow">{{$listing->user->phone}}</td>
                                            <td style="background-color: yellow">{{$listing->amount}}</td>
                                            <td style="background-color: yellow">{{$listing->user->bank}} | {{$listing->user->account}}</td>yellow
                                             <td style="background-color: yellow">{{$listing->type}}</td>
                                            <td style="background-color: yellow">{{$listing->created_at}}</td>


                                            <td style="background-color: yellow"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>


                                    @else



                @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*19)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*19)))
@endif

 @if ($maturitydate->isPast())
  @if(!$listing->comments->count())
        <td style="background-color: blue">{{$listing->id}} GH (MATURED)
             </td>
     @else
<td style="background-color: #f85e00">{{$listing->id}} GH (MATCHED)
             </td>
     @endif                                       <td style="background-color: blue">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: blue">{{$listing->user->email}}</td>
                                            <td style="background-color: blue">{{$listing->user->phone}}</td>
                                            <td style="background-color: blue">{{$listing->amount}}</td>
                                            <td style="background-color: blue">{{$listing->user->bank}} | {{$listing->user->account}}</td>
                                             <td style="background-color:blue">{{$listing->type}}</td>
                                            <td style="background-color: blue">{{$listing->created_at}}</td>


                                            <td style="background-color: blue"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>

  @else
             @if(!$listing->comments->count())
           <td style="background-color: #ae759f">{{$listing->id}} GH (MATURING)
             </td>
             @else
                <td style="background-color: #f85e00">{{$listing->id}} GH (MATCHED)
             </td>
             @endif
                                            <td style="background-color: #ae759f">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: #ae759f">{{$listing->user->email}}</td>
                                            <td style="background-color: #ae759f">{{$listing->user->phone}}</td>
                                            <td style="background-color: #ae759f">{{$listing->amount}}</td>
                                            <td style="background-color: #ae759f">{{$listing->user->bank}} | {{$listing->user->account}}</td>
                                             <td style="background-color: #ae759f">{{$listing->type}}</td>
                                            <td style="background-color: #ae759f">{{$listing->created_at}}</td>


                                            <td style="background-color: #ae759f"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>

  @endif


                                    @endif
                                        </tr>

                                           @foreach ($listing->comments as $comment)
                                           <tr>
                                                <td>{{$comment->user->name}} {{$comment->user->surname}}</td>
                                                <td>{{$comment->user->email}}</td>
                                                <td>{{$comment->user->phone}}</td>
                                                <td>{{$comment->split}}</td>
                                                <td>{{$comment->created_at}}</td>
                                                <td>@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Paid</button>
                                                @else
                                                      <button type="submit" class="btn btn-warning">Not Paid</button>
                                                @endif

                                            </td>
                                            <td>
                                              <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $comment->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Reallocate Coins"><i class="fe fe-trash"></i>Reallocate</a></li>

                             <form action="{{route('admin.comment.destroy', [$comment->id])}}" method="post" id="comment-destroy-form-{{ $comment->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>


                                            </td>
                                             </tr>
                                           @endforeach


                                        @endforeach

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->



    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="assets/js/app.js"></script>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="plugins/table/datatable/datatables.js"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

<!-- Mirrored from designreset.com/cork/ltr/demo3/table_dt_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Sep 2020 15:56:52 GMT -->
</html>
