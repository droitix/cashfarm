@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
<div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">

                        <h1 class="">Register</h1>
                        <p class="signup-link register">Already have an account? <a href="{{ route('login') }}">Log in</a></p>
                          <form class="text-left" action="{{ route('register') }}" method="POST" autocomplete="off">
                                 @csrf
                            <div class="form">

                                <div id="username-field" class="field-wrapper input">


                                   <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" type="text" value="{{ old('name') }}" required autofocus  placeholder="First Name">
                                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                </div>

                                 <div id="username-field" class="field-wrapper input">

                                 <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" type="text" value="{{ old('surname') }}" required autofocus  placeholder="Last Name">
                                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                </div>

                                  <div id="username-field" class="field-wrapper input">


                                  <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" type="text" value="{{ old('phone') }}" required autofocus  placeholder="Cell Number">
                                         @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                </div>

                                  <div id="username-field" class="field-wrapper input">
                                        <select name="bank" class="form-control">
                                        
                                          <option value="M-PESA">M-PESA</option>
                                       

                                        </select>
                                </div>


                                  <div id="username-field" class="field-wrapper input">

                                        <input class="form-control{{ $errors->has('account') ? ' is-invalid' : '' }}" name="account" type="text" value="{{ old('account') }}" required autofocus  placeholder="Account Number">
                                         @if ($errors->has('account'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('account') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <div id="email-field" class="field-wrapper input">

                                     <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <div id="password-field" class="field-wrapper input mb-2">
                                    <div class="d-flex justify-content-between">


                                    </div>

                                   <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                </div>

                                <div id="password-field" class="field-wrapper input mb-2">
                                 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">
                                </div>


                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" class="btn btn-primary" value="">Create Account</button>
                                    </div>
                                </div>


                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
