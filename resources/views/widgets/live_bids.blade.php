   





   <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

  @include('layouts.partials.sidebar')



  @if (session()->has('impersonate'))
 <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row sales layout-top-spacing">

                @foreach ($listings as $listing)

 @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*20)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*20)))
@endif

 @if ($maturitydate->isPast())

 @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)



                                  @endforeach

             @if ($listing->amount > $sum)

                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                        <div class="widget widget-account-invoice-two">
                            <div class="widget-content">
                                <div class="account-box">
                                    <div class="info">

                                        <h5 class="">{{$listing->user->bank}}</h5>
                                        <h5 class="">{{$listing->user->email}}</h5>
                                        <h5 class="inv-balance">R {{$listing->amount - $sum}}</h5>
                                    </div>
                                    <div class="acc-action">
                                        <div class="">
                                            <a href="{{ route('listings.apply', [$area, $listing]) }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>

                                        </div>
                                        <a href="{{ route('listings.apply', [$area, $listing]) }}">Select Bid</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
 @else


            @endif


            @elseif($listing->type())

 @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)



                                  @endforeach

             @if ($listing->amount > $sum)
      <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                        <div class="widget widget-account-invoice-two">
                            <div class="widget-content">
                                <div class="account-box">
                                    <div class="info">

                                        <h5 class="">{{$listing->user->bank}}</h5>
                                        <h5 class="">{{$listing->user->email}}</h5>
                                        <h5 class="inv-balance">R {{$listing->amount - $sum}}</h5>
                                    </div>
                                    <div class="acc-action">
                                        <div class="">
                                            <a href="{{ route('listings.apply', [$area, $listing]) }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>

                                        </div>
                                        <a href="{{ route('listings.apply', [$area, $listing]) }}">Select Bid</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


    @else


            @endif


            @else


@endif

@endforeach


                    </div>
                </div>

            </div>

        </div>
        <!--  END CONTENT AREA  -->
        @else

 <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row sales layout-top-spacing">



                    <h3>PAIRING AT 10AM AND 6PM DAILY </h3>




                    </div>
                </div>

            </div>

        </div>
        <!--  END CONTENT AREA  -->


        @endif




   </div>
    <!-- END MAIN CONTAINER -->
