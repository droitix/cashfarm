@extends('layouts.app')

@section('title', 'Taken Bid')

@section('description')

@endsection

@section('content')
<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>

                            <li class="active">
                                <a href="index.html"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                            </li>
                            @role('admin')
                            <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                              <li>
                                <a href="{{ url('admin/impersonate')}}"><i class="fe fe-lock"></i> <span>Impersonate</span></a>
                            </li>
                            @endrole
                            @if (session()->has('impersonate'))
                              <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                            @endif
                            @if (session()->has('impersonate'))
                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="fe fe-lock"></i> Stop Impersonating</a>
                        </li>
                        <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

              @endif

                            <li>
                                <a href="{{url('bidding')}}"><i class="fe fe-bell"></i> <span>Bidding Rooms</span></a>
                            </li>
                             <li>
                                <a href="{{ route('comments.published.index') }}"><i class="fe fe-document"></i> <span>My Bids</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fe fe-tiled"></i> <span>Banked Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.published.index', [$area]) }}"><i class="fe fe-money"></i> <span>Selling Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.history.index', [$area]) }}"><i class="fe fe-file"></i> <span>All Transactions</span></a>
                            </li>


                            <li>
                                <a href="{{ route('profile') }}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
                            </li>
                                <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fe fe-logout"></i>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Sidebar -->


    <!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

                    <div class="row">


                      <h4>Looks like bid is taken,Check your account if you won the bid.....go click on (my bids) <a href="{{ route('comments.published.index') }}" type="submit" class="btn btn-warning">My Bids<span></span></a></h4>

                    </div>

                </div>
            </div>
            <!-- /Page Wrapper -->












@endsection
