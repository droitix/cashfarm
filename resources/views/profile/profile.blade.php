@extends('layouts.app')
@section('title', 'Profile')

@section('description')

@endsection
@section('content')

 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="account-settings-container layout-top-spacing">

                    <div class="account-content">
                        <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                            <div class="row">





                                <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                    <form action="{{route('profile.update')}}" method="POST">
                                        @csrf
                                        <div class="info">
                                            <h5 class="">My Profile</h5>
                                            <div class="row">
                                                <div class="col-md-11 mx-auto">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="country">First Name</label>
                                                               <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="address">Last Name</label>
                                                               <input type="text" class="form-control" name="surname" value="{{$user->surname}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="location">Email</label>
                                                                 <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone</label>
                                                                 <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="email">Bank Name</label>
                                                                 <input type="text" class="form-control" name="bank" value="{{$user->bank}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="website1">Account Number</label>
                                                                <input type="text" class="form-control" name="account" value="{{$user->account}}">
                                                            </div>
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="phone">Branch Code</label>
                                                                 <input type="text" class="form-control" name="branch" value="{{$user->branch}}">
                                                            </div>
                                                        </div>
                                                       
                                                  
                                               
                                                        <input type="hidden" class="form-control" name="btcaddress" value="{{$user->btcaddress}}">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="email">Account Type</label>
                                                                 <input type="text" class="form-control" name="accounttype" value="{{$user->accounttype}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                            <button id="multiple-messages" class="btn btn-dark">Save Changes</button>
                                    </form>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="account-settings-footer">

                        <div class="as-footer-container">




                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--  END CONTENT AREA  -->

</div>
</div>
</div>
@endsection
