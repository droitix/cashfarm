<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.head')

</head>
<body>

          <div id="app">
   @include('layouts.partials.regnav')


<div class="wrapper">
    <div class="preloader"></div>

       @yield('content')




           </div>


    @include('layouts.partials.regpopup')
@include('sweetalert::alert')


@include('notify::messages')
@notifyJs
<a class="scrollToHome" href="#"><i class="flaticon-rocket-launch"></i></a>
</div>
<!-- Wrapper End -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/6052e3af067c2605c0b9aa31/1f11siqd9';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>

<script type="text/javascript" src="/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="/js/popper.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/snackbar.min.js"></script>
<script type="text/javascript" src="/js/simplebar.js"></script>
<script type="text/javascript" src="/js/parallax.js"></script>
<script type="text/javascript" src="/js/scrollto.js"></script>
<script type="text/javascript" src="/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="/js/jquery.counterup.js"></script>
<script type="text/javascript" src="/js/wow.min.js"></script>
<script type="text/javascript" src="/js/progressbar.js"></script>
<script type="text/javascript" src="/js/slider.js"></script>
<script type="text/javascript" src="/js/timepicker.js"></script>
<!-- Custom script for all pages -->
<script type="text/javascript" src="/js/script.js"></script>




</html>
