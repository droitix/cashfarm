@extends('layouts.app')

@section('title', 'Live Coins')

@section('description')

@endsection

@section('content')



 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row layout-top-spacing">
                    @if ($listings->count())
        @each ('listings.partials.listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">You Currently have no coins</p></th>
    @endif
                </div>


        </div>
        <!--  END CONTENT PART  -->

    </div>
    <!-- END MAIN CONTAINER -->


@endsection
