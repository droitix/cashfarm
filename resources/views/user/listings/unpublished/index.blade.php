@extends('layouts.app')
@section('title', 'Create')

@section('description')

@endsection
@section('content')

 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="account-settings-container layout-top-spacing">

                    <div class="account-content">
                        <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                            <div class="row">




                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="table-responsive mb-4 mt-4">
                                <table class="multi-table table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Maturity Period</th>
                                            <th>Maturity Price</th>
                                            <th>Action</th>

                                            <th>Admin</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                                                  @if ($listings->count())
        @each ('listings.partials._listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">You Currently have no coins</p></th>
    @endif

                                    </tbody>

                                </table>



</div>
</div>
</div>


                        </div>
                    </div>

                    <div class="account-settings-footer">

                        <div class="as-footer-container">




                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--  END CONTENT AREA  -->

</div>
</div>
</div>
@endsection
