@extends('layouts.app')

@section('title', 'Live Coins')

@section('description')

@endsection

@section('content')



 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row layout-top-spacing">
                    @if ($comments->count())
        @each ('listings.partials._comment_own', $comments, 'comment')
        {{ $comments->links() }}
    @else
       <th>  <p style="text-align: center;">YOU HAVE NOT BEEN PAIRED YET.</p></th>
    @endif

                </div>


        </div>
        <!--  END CONTENT PART  -->

    </div>
    <!-- END MAIN CONTAINER -->


@endsection
