@extends('layouts.app')

@section('title', 'Sell')

@section('description')

@endsection

@section('content')



 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row layout-top-spacing">

                               <div id="basic" class="col-lg-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">

                                        @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif

                                  @endforeach
                                            <h4>Total Bid amount is R{{$listing->amount - $sum}} to {{$listing->user->bank}}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">

                                    <div class="row">
                                        <div class="col-lg-6 col-12 mx-auto">
                                            <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                                                <div class="form-group">
                                                     @if(session('success'))
    <h3 style="color:green">{{session('success')}}</h3>
@endif

                                                     <div class=" col-lg-12">
                                                 @include('listings.partials.forms.categories')
                                                    </div>
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Bidded">
                                       <div class="form-group{{ $errors->has('split') ? ' has-error' : '' }}">
                                            <label>Enter amount you can afford from R300 to R{{$listing->amount - $sum}}</label>
                                            <input type="text" name="split" class="form-control">

                                              @if ($errors->has('split'))
                                <span class="help-block">
                                    {{ $errors->first('split') }}
                                </span>
                            @endif
                                        </div><br>
                                                    <input type="submit" name="txt" class="mt-4 btn btn-primary">
                                                </div>
                                                 {{ csrf_field() }}
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

            </div>
        </div>

        </div>
        <!--  END CONTENT PART  -->

    </div>
    <!-- END MAIN CONTAINER -->


@endsection
