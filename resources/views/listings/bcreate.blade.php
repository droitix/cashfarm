@extends('layouts.app')
@section('title', 'Create')

@section('description')

@endsection
@section('content')

 <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

@include('layouts.partials.sidebar')

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="account-settings-container layout-top-spacing">

                    <div class="account-content">
                        <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                            <div class="row">



                                 <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Create Bid</h4>
                                </div>
                                <div class="card-body">
                                 <form action="{{ route('listings.store', [$area]) }}" method="post">
                                        <div class=" col-lg-12">
                              @include('listings.partials.forms._categories')
                                        </div>

                                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                            <label>Amount(KWACHA)</label>
                                            <input type="text" name="amount" class="form-control">

                                              @if ($errors->has('amount'))
                                <span class="help-block">
                                    {{ $errors->first('amount') }}
                                </span>
                            @endif
                                        </div>
                                        <input type="hidden" class="form-control" name="type" id="type" value="1">
                                         <input type="hidden" class="form-control" name="bitcoin" id="bitcoin" value="0">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-primary">Create</button>

                                         {{ csrf_field() }}
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>




                        </div>
                    </div>

                    <div class="account-settings-footer">

                        <div class="as-footer-container">




                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--  END CONTENT AREA  -->

</div>
</div>
</div>
@endsection
