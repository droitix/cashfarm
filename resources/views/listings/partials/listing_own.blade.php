 @if($listing->bitcoin())

 @if($listing->type())

                             @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*18)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*18)))
@endif

                           <div class="col-md-4">
                                            <div class="card-pricing text-center card pb-4 mt-4">
                                                 @if (!$maturitydate->isPast())
                                                <div class="pricing-header bg-primary rounded-top">
                                                    <h1 class="card-price text-white pt-4">$ {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">Maturing Transaction</h5>


                                                </div>
                                                 @else

                                                 @if(!$listing->recommit())

                                                  <div class="pricing-header bg-warning rounded-top">
                                                    <h1 class="card-price text-white pt-4">$ {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">Transation Matured</h5>
                                                     <h5 class="name text-white pb-4">Status: Recommit</h5>

                                                </div>
                                                @else
                                                   <div class="pricing-header bg-success rounded-top">
                                                    <h1 class="card-price text-white pt-4">$ {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">Transation Matured</h5>
                                                     <h5 class="name text-white pb-4">Status: Ready for Withdrawal</h5>

                                                </div>

                                                @endif



                                                  @endif
                                                <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                    <li><h5>GH12{{$listing->id}}</h5></li>
                                                      @if ($listing->comments->count())
                                                    <li><h5>Details below will pay you</h5></li>
                                                      @foreach($listing->comments as $comment)
                                                      <hr>
                                                    <li>{{$comment->user->name}} {{$comment->user->surname}}</li>
                                                    <li>{{$comment->user->phone}}</li>
                                                    <li>Amount: ${{$comment->split}}</li>
                                                     @if ($listing->comments->count())
                                                     @if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Approved {{$comment->user->name}}</button>
                                                   @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <button type="submit" class="btn btn-primary">Confirm {{$comment->user->name}} Paid</button>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif



                                               @else

                                               @endif

                                                @endforeach

                                                  @else
                               @if(!$listing->recommit())
                             <li><h5>Recommit Countdown: <p id="demo"></p></h5></li>
                             <!-- Display the countdown timer in an element -->


<script>
// Set the date we're counting down to
var countDownDate = new Date("{{$maturitydate}}").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "MATURED";
  }
}, 1000);
</script>
                                @else
                             <li><h5>Recommit Done: System is Matching you for withdrawal</h5></li>
                                @endif
                             @if ($maturitydate->isPast())
                                @if(!$listing->recommit())
                              <a href="{{ route('listings.create', [$area]) }}" class="btn btn-primary">Recommit ${{$listing->amount}} Here</a>
                              @else

                              @endif
                               <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                          <input type="hidden" class="form-control" name="recommit" id="recommit" value="1">
                                          <input type="hidden" class="form-control" name="matched" id="matched" value="{{$listing->matched}}">
                                            @if (session()->has('impersonate'))
                                               @if(!$listing->recommit())
                                             <button type="submit" class="btn btn-success">Admin confirm Recommit</button>
                                             @else

                                             @endif
                                          @endif


                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>
                             @endif

                              @endif


                                            </div> <!-- end Pricing_card -->
                                        </div> <!-- end col -->
@else

                           <div class="col-md-4">
                                            <div class="card-pricing text-center card pb-4 mt-4">
                                                 @if($listing->matched())
                                                  <div class="pricing-header bg-success rounded-top">
                                                    <h1 class="card-price text-white pt-4">$ {{$listing->amount}}</h1>
                                                    <h5 class="name text-white pb-4">This pledge has been matched</h5>
                                                </div>

                                                 @else
                                                <div class="pricing-header bg-danger rounded-top">
                                                    <h1 class="card-price text-white pt-4">$ {{$listing->amount}}</h1>
                                                    <h5 class="name text-white pb-4">ORDER</h5>
                                                </div>
                                                @endif

                                                <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                    <li><h3>WD{{$listing->id}}</h3></li>
                                              @if($listing->matched())
                                                <li><h5>ORDER PAIRED. Check <a href="{{ route('comments.published.index') }}">Here</a></h5> </li>
                                              @else
                                                    <li><h5>ORDER IS BEING PAIRED</h5></li>
                                              @endif
                                               @if (session()->has('impersonate'))
                                               @if(!$listing->matched())
                                               <a href="{{ url('bidding')}}"  class="btn btn-warning">Admin Match this request</a><br>
                                               @else

                                               @endif
                                               @endif
                                        <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                          <input type="hidden" class="form-control" name="matched" id="matched" value="1">
                                          <input type="hidden" class="form-control" name="recommit" id="recommit" value="{{$listing->recommit}}">
                                            @if (session()->has('impersonate'))
                                             @if(!$listing->matched())
                                            <button type="submit" class="btn btn-primary">Confirm Request Matched</button>
                                            @else
                                             <button type="submit" class="btn btn-success">Request Matched</button>
                                            @endif
                                            @endif

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>


    <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
@if (session()->has('impersonate'))
<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>
@endif
                                                 </ul>





                                            </div> <!-- end Pricing_card -->
                                        </div> <!-- end col -->





@endif







 @else



 @if($listing->type())

                             @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*18)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*18)))
@endif

                           <div class="col-md-4">
                                            <div class="card-pricing text-center card pb-4 mt-4">
                                                 @if (!$maturitydate->isPast())
                                                <div class="pricing-header bg-primary rounded-top">
                                                    <h1 class="card-price text-white pt-4">Ksh {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">THANK YOU! WAIT TO BE PAIRED</h5>


                                                </div>
                                                 @else

                                                 @if(!$listing->recommit())

                                                  <div class="pricing-header bg-warning rounded-top">
                                                    <h1 class="card-price text-white pt-4">Ksh {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">READY FOR PAYOUT</h5>
                                                     <h5 class="name text-white pb-4">Status: WAIT FOR PAIR</h5>

                                                </div>
                                                @else
                                                   <div class="pricing-header bg-success rounded-top">
                                                    <h1 class="card-price text-white pt-4">Ksh {{$listing->amount}}</h1>


                                                     <h5 class="name text-white pb-4">Transation Matured</h5>
                                                     <h5 class="name text-white pb-4">Status: Ready for Withdrawal</h5>

                                                </div>

                                                @endif



                                                  @endif
                                                <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                    <li><h5>WD{{$listing->id}}</h5></li>
                                                      @if ($listing->comments->count())
                                                    <li><h5>Details below will pay you</h5></li>
                                                      @foreach($listing->comments as $comment)
                                                      <hr>
                                                    <li>{{$comment->user->name}} {{$comment->user->surname}}</li>
                                                    <li>{{$comment->user->phone}}</li>
                                                    <li>Amount: K{{$comment->split}}</li>
                                                     @if ($listing->comments->count())
                                                     @if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Approved {{$comment->user->name}}</button>
                                                   @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <button type="submit" class="btn btn-primary">Confirm {{$comment->user->name}} Paid</button>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif



                                               @else

                                               @endif

                                                @endforeach

                                                  @else
                               @if(!$listing->recommit())
                             <li><h5>PAYOUT: <p id="demo"></p></h5></li>
                             <!-- Display the countdown timer in an element -->


<script>
// Set the date we're counting down to
var countDownDate = new Date("{{$maturitydate}}").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "MATURED";
  }
}, 1000);
</script>
                                @else
                             <li><h5>Recommit Done: System is Matching you for withdrawal</h5></li>
                                @endif
                             @if ($maturitydate->isPast())
                                @if(!$listing->recommit())
                            
                              @else

                              @endif
                               <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                          <input type="hidden" class="form-control" name="recommit" id="recommit" value="1">
                                          <input type="hidden" class="form-control" name="matched" id="matched" value="{{$listing->matched}}">
                                            @if (session()->has('impersonate'))
                                               @if(!$listing->recommit())
                                             <button type="submit" class="btn btn-success">Admin confirm Recommit</button>
                                             @else

                                             @endif
                                          @endif


                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>
                             @endif

                              @endif


                                            </div> <!-- end Pricing_card -->
                                        </div> <!-- end col -->
@else

                           <div class="col-md-4">
                                            <div class="card-pricing text-center card pb-4 mt-4">
                                                 @if($listing->matched())
                                                  <div class="pricing-header bg-success rounded-top">
                                                    <h1 class="card-price text-white pt-4">Ksh {{$listing->amount}}</h1>
                                                    <h5 class="name text-white pb-4">PAIRED ORDER</h5>
                                                </div>

                                                 @else
                                                <div class="pricing-header bg-danger rounded-top">
                                                    <h1 class="card-price text-white pt-4">Ksh {{$listing->amount}}</h1>
                                                    <h5 class="name text-white pb-4">ORDER</h5>
                                                </div>
                                                @endif

                                                <ul class="list-unstyled card-pricing-features text-muted pt-3">
                                                    <li><h3>WD12{{$listing->id}}</h3></li>
                                              @if($listing->matched())
                                                <li><h5>ORDER PAIRED. Check <a href="{{ route('comments.published.index') }}">Here</a></h5> </li>
                                              @else
                                                    <li><h5>THANK YOU! WAIT TO BE PAIRED</h5></li>
                                              @endif
                                               @if (session()->has('impersonate'))
                                               @if(!$listing->matched())
                                               <a href="{{ url('bidding')}}"  class="btn btn-warning">Admin Match this request</a><br>
                                                <h5>{{$listing->category->parent->color}}</h5>
                                               @else

                                               @endif
                                               @endif
                                        <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                          <input type="hidden" class="form-control" name="matched" id="matched" value="1">
                                          <input type="hidden" class="form-control" name="recommit" id="recommit" value="{{$listing->recommit}}">
                                            @if (session()->has('impersonate'))
                                             @if(!$listing->matched())
                                            <button type="submit" class="btn btn-primary">Confirm Request Matched</button>

                                            @else
                                             <button type="submit" class="btn btn-success">Request Matched</button>
                                            @endif
                                            @endif

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>


    <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
@if (session()->has('impersonate'))
<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>
@endif
                                                 </ul>





                                            </div> <!-- end Pricing_card -->
                                        </div> <!-- end col -->





@endif




@endif
