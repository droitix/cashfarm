
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                         @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*23)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*23)))
@endif

                                        <h3 class="card-title">K {{$listing->amount}} </h3>   <h4> {{$listing->category->parent->color}} Day Bid</h4>
                                        <h4> Matures {{$maturitydate}}</h4>


                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Bid Status</th>
                                                        <th>Bid Owner</th>
                                                        <th>Contact</th>
                                                        <th>Bid Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       @if ($listing->comments->count())
                                    @foreach ($listing->comments as $comment)
                                                 <tr class="table-light">
                                                        <th scope="row">@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Approved</button>
                                                @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <button type="submit" class="btn btn-primary">Confirm</button>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif
                                          </th>
                                                        <td>{{$comment->user->email}} </td>
                                                        <td>{{$comment->user->phone}}</td>
                                                        <td>{{$comment->split}}</td>
                                                    </tr>
                                                     @endforeach

                              @else
                                <h5>Bid Scheduled for auction</h5>

                              @endif
                                                     </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>




