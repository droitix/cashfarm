


                <div class="col-xl-5 col-lg-12 col-md-6 col-sm-12 col-12 layout-spacing">
                        <div class="widget widget-table-one">
                            <div class="widget-heading">

                                @if ($comment->approvals->count())
                                   <h5 class="">Paid</h5>


                                 @else
                                  <h5 class="">Payment not Confirmed</h5>
                                 @endif
                            </div>

                             <div class="acc-total-info">
                                        <h5>You PAY Ksh{{$comment->split}}</h5>
                                        <h3>Will mature as Ksh{{($comment->category->parent->percent*$comment->split)}}</h3>
                             </div>



                            <div class="widget-content">
                                <div class="transactions-list">
                                    <div class="t-item">
                                        <div class="t-company-name">
                                            <div class="t-icon">
                                                <div class="icon">

                                                </div>
                                            </div>
                                            <div class="t-name">
                                                <h4>{{$comment->listing->user->name}}</h4>
                                                <p class="meta-date">{{$comment->listing->user->phone}}</p>
                                            </div>&nbsp;&nbsp;
                                             <div class="t-name">
                                                @if ($comment->approvals->count())
                                                 <form action="{{ route('listings.store', [$area]) }}" method="post">

                                           <input type="hidden" class="form-control" name="amount" id="amount" value="{{($comment->category->parent->percent*$comment->split)}}">
                                           <input type="hidden" class="form-control" name="maturityamount" id="maturityamount" value="{{($comment->category->parent->percent*$comment->split)}}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="type" id="type" value="1">
                                      <input type="hidden" class="form-control" name="category_id" id="category" value="{{$comment->category_id}}">
                                         <input type="hidden" class="form-control" name="secret" id="category" value="{{$comment->id}}">
                                          <input type="hidden" class="form-control" name="updated_at" id="created" value="{{$comment->created_at}}">

                                            <button type="submit" class="btn btn-success">View Maturity</button>

                                         {{ csrf_field() }}
                                    </form>
                                                @else
                                            <a href="{{ route('comments.show', [$area,$comment]) }}" type="button" class="btn btn-primary">View Payment Details</a>
                                                @endif

                                                 @if (session()->has('impersonate'))

                                                <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $comment->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Bid"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('comment.destroy', [$area,$comment->id])}}" method="post" id="comment-destroy-form-{{ $comment->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>

                                    @endif

                                            </div>

                                        </div>

                                    </div>
                                </div>


                            </div>


                        </div>
                    </div>
